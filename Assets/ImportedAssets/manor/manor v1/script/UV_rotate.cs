using UnityEngine;
using System.Collections;

public class UV_rotate : MonoBehaviour {

    public Mesh MyMesh;
	private Vector2[] RotatedUVs;
	public int rotation_angle;
     
    void Awake() 
		{
		MeshFilter mf = GetComponent<MeshFilter>();
		MyMesh = mf.mesh;
        RotatedUVs  = MyMesh.uv;
       
       
        for (var i = 0 ; i < RotatedUVs.Length; i++)
			{
            RotatedUVs[i] += new Vector2(1,1);
        	var rot = Quaternion.Euler(0,0,rotation_angle);
            RotatedUVs[i] = rot * RotatedUVs[i];
        	}
       
        MyMesh.uv = RotatedUVs;
   	 	}
}
