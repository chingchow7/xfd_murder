﻿using UnityEngine;
using System.Collections;

public class HK_RigidbodyMovement : MonoBehaviour {

	public float walkAcceleration;
	public float walkAccelAirRacio;
	public float walkDeacceleration;
	public float maxWalkSpeed;
	public float jumpVelocity;
	public float maxSlope;
	
	public GameObject cameraObject;

	//ANIMTION VARIABLES
	public int AnimationSpeed;
	public bool AnimationJumpBool;
	
	float walkDeaccelerationVolx;
	float walkDeaccelerationVolz;
	Vector2 horizontalMovement;
	bool grounded;
	
	float tempRigidx;
	float tempRigidz;
	
	// Use this for initialization
	void Start () {
		walkAcceleration = 10f;
		walkAccelAirRacio = .1f;
		walkDeacceleration = .5f;
		maxWalkSpeed = 20f;
		jumpVelocity = 150f;
		maxSlope = 60f;
		
		grounded = false;
	}
	
	void LateUpdate () {
		//  Convert velocity to vector 2 and set as horizontal movement
		horizontalMovement = new Vector2(rigidbody.velocity.x, rigidbody.velocity.z);
		
		// Take magnitude of vector and check to see if its greater than max movement speed
		if ((horizontalMovement.magnitude > maxWalkSpeed) && grounded)  {
			horizontalMovement = horizontalMovement.normalized;
			horizontalMovement *= maxWalkSpeed;     
		}

		
		if ( grounded ){
			//rigidbody.velocity.x = Mathf.SmoothDamp(rigidbody.velocity.x, 0, ref walkDeaccelerationVolx, walkDeacceleration);
			//rigidbody.velocity.z = Mathf.SmoothDamp(rigidbody.velocity.z, 0, ref walkDeaccelerationVolz, walkDeacceleration);
			tempRigidx = Mathf.SmoothDamp(rigidbody.velocity.x, 0, ref walkDeaccelerationVolx, walkDeacceleration);
			tempRigidz = Mathf.SmoothDamp(rigidbody.velocity.z, 0, ref walkDeaccelerationVolz, walkDeacceleration);
			rigidbody.velocity = new Vector3(tempRigidx, rigidbody.velocity.y, tempRigidz);
		}
		
		// Move object torwards mouse angle
		transform.rotation = Quaternion.Euler(0, (float) cameraObject.GetComponent<HK_MouseLook>().currentYRotation, 0);
		
		
		
		// Apply force if on ground but reduce this influence if you are in air - Walking
		if (grounded)
			rigidbody.AddRelativeForce(Input.GetAxis("Horizontal") * walkAcceleration, 0, Input.GetAxis("Vertical") * walkAcceleration);     
		else
			rigidbody.AddRelativeForce(Input.GetAxis("Horizontal") * walkAcceleration * walkAccelAirRacio, 0, Input.GetAxis("Vertical") * walkAcceleration * walkAccelAirRacio);
		
		// If "jump" is pressed and you are on ground. Jump
		if (Input.GetButtonDown("Jump") && grounded) {
			rigidbody.AddForce(0,jumpVelocity,0);
			AnimationJumpBool = true;
		} else {
			AnimationJumpBool = false;
		}

		//SETTING ANIMTION TRANSITION VARIABLES
		if ( horizontalMovement.magnitude < 1.4) {
			AnimationSpeed = 0;
		} else {
			AnimationSpeed = 10;
		}

		//Debug.Log (rigidbody.velocity.z);

		
	}
	
	// function checks for if player is on ground
	void OnCollisionStay (Collision collision) {
		
		foreach (ContactPoint contact in collision.contacts) {
			
			if (Vector3.Angle(contact.normal, Vector3.up) < maxSlope)
				grounded = true;

			
		}
	}
	
	// If collision with floor is not detected. We are no longer grounded
	void OnCollisionExit ()  {
		
		grounded = false;
		
	}
}
