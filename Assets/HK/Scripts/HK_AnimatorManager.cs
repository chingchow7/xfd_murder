﻿using UnityEngine;
using System.Collections;

public class HK_AnimatorManager : MonoBehaviour {

	int AnimSpeed = 0;
	bool AnimJump = false;
	Animator Anim;
	HK_RigidbodyMovement RigidbodyController;

	// Use this for initialization
	void Start () {
		Anim = GetComponent<Animator>();
		RigidbodyController = GetComponent<HK_RigidbodyMovement>();
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (AnimJump);
		AnimSpeed = RigidbodyController.AnimationSpeed;
		AnimJump = RigidbodyController.AnimationJumpBool;

		Anim.SetInteger("Speed", AnimSpeed);
		Anim.SetBool ("Jumping", AnimJump);
	}

}
