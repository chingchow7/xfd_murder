﻿using UnityEngine;
using System.Collections;

public class HK_MouseLook : MonoBehaviour {

	public float lookSensitivity;
	public float lookSmoothDamp;
	
	float yRotation;
	float xRotation;
	public float currentYRotation;
	public float currentXRotation;
	float yRotationV;
	float xRotationV;
	
	// Use this for initialization
	void Start () {
		lookSensitivity = 5f;
		lookSmoothDamp = .1f;
	}
	
	// Update is called once per frame
	void Update () {
		
		// Get mouse input and save as rotation
		yRotation += Input.GetAxis("Mouse X") * lookSensitivity;
		xRotation -= Input.GetAxis("Mouse Y") * lookSensitivity;
		
		// Place a limit on Y up and down look  
		xRotation = Mathf.Clamp(xRotation, -90, 90);
		
		// Smoothly rotate
		currentXRotation = Mathf.SmoothDamp(currentXRotation, xRotation, ref xRotationV, lookSmoothDamp);
		currentYRotation = Mathf.SmoothDamp(currentYRotation, yRotation, ref yRotationV, lookSmoothDamp);
		
		// output rotational value using euler method
		transform.rotation = Quaternion.Euler(currentXRotation, currentYRotation, 0);
		
	}
}
